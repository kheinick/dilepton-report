\input{head.tex}

\title{%
    Studies of Top-Quark-Pair Kinematic Reconstructions in the Dilepton-channel
}
\subtitle{%
    Summer Student Project Report
}
\author{%
    Kevin Heinicke\\
    \texttt{\href{mailto:kevin.heinicke@cern.ch}{kevin.heinicke@cern.ch}}\\
    \\
    Supervisor: Tancredi Carli\\
    \texttt{\href{mailto:tancredi.carli@cern.ch}{tancredi.carli@cern.ch}}
}
\date{%
    September 11, 2015
}
\begin{document}
    \maketitle%

    \section{Introduction to $t\overline{t}$-Dilepton-events}
    \label{sec:introduction}
    The improved measurement of top-quark properties is
    one major goal of data analysis at the ATLAS experiment during run 2 of the LHC.
    Top-quarks are mostly produced in pairs via the strong interaction.
    They decay almost exclusively $W^\pm$-bosons and b-Quarks and therefore are
    classified by the $W^\pm$-decay.
    One of these modes is the
    Dilepton-channel, in which both $W$-bosons transform into a
    lepton-neutrino-pair as shown in figure \ref{fig:dilepton-diagram}
    in leading order of the strong interaction.
    \begin{figure}
        \centering
        \begin{tikzpicture}
            \coordinate (g1b) at (-1.5,0);
            \coordinate (g2b) at (-1.5,2);
            \coordinate (t1b) at (0,0);
            \coordinate (t2b) at (0,2);
            \coordinate (t1e) at (1.5,0);
            \coordinate (t2e) at (1.5,2);
            \coordinate (b1e) at (3.5,0.5);
            \coordinate (b2e) at (3.5,1.5);
            \coordinate (w1e) at (2.5,-0.5);
            \coordinate (w2e) at (2.5,2.5);
            \coordinate (nu1e) at (3.5,-0.25);
            \coordinate (nu2e) at (3.5,2.25);
            \coordinate (l1e) at (3.5,-1.0);
            \coordinate (l2e) at (3.5,3);

            \draw[gluon] (g1b) node[left]{$g$} -- (t1b);
            \draw[gluon] (g2b) node[left]{$g$} -- (t2b);

            \draw[electron] (t2b) -- (t1b);

            \draw[electron] (t1b) -- node[below]{$t$} (t1e);
            \draw[electron] (t2e) -- node[above]{$\overline{t}$} (t2b);

            \draw[electron] (t1e) -- (b1e) node[right]{$b$};
            \draw[electron] (b2e) node[right]{$\overline{b}$} -- (t2e);

            \draw[vector] (t1e) -- node[below]{$W^+$} (w1e);
            \draw[vector] (w2e) -- node[above]{$W^-$} (t2e);

            \draw[electron] (w2e) -- (l2e) node[right]{$\ell^-$};
            \draw[electron] (nu2e) node[right]{$\overline{\nu}$} -- (w2e);

            \draw[electron] (w1e) -- (l1e) node[right]{$\ell^+$};
            \draw[electron] (nu1e) node[right]{$\nu$} -- (w1e);
        \end{tikzpicture}
        \caption{
            Feynman-graph of $t\overline{t}$-Dilepton-events in leading order QCD.
        }
        \label{fig:dilepton-diagram}
    \end{figure}
    The presence of two neutral leptons $\nu_1$ and $\nu_2$ is a challenge
    for the event reconstruction.
    Since both leptons have almost no interactions with the detector, their 4-momenta
    cannot be directly measured and only the
    sum of their transverse momenta $E_\text{t}^\text{miss}$ can be derived.
    Assuming that there are no other high-momentum neutral leptons in the
    respective event, $\nu_1$ and $\nu_2$ are constrained by this missing transverse energy.
    Furthermore the charge of the $b$-quark is difficult to measure in the ATLAS
    detector, which results in several possible combinations of leptons and
    $b$-Quarks, depending on the number of charged leptons and tagged $b$-quarks
    in the corresponding event.

    To be able to improve the reconstruction of those events, a good
    understanding of the reconstruction methods is neccessary.
    This project aims to improve the understanding of the neutrino-weighting
    method shown in section \ref{sec:neutrino-weighting} by implementing such a
    method and testing it with events produced in leading order of the strong
    coupling using a QCD calculation based on SHERPA \cite{sherpa} monte carlo.
    The studie is done with the leading order partons, no attempt is made
    to include additional radiation or hadronization.
    We use all events without application of event selection criteria to
    emulate the detectors behaviour.

    The projects program code can be found at
    \texttt{\url{http://gitlab.cern.ch/kheinick/dilepton}}.

    \section{Neutrino-Weighting method}
    \label{sec:neutrino-weighting}
    The neutrino-weighting method examined in this project reconstructs
    the top-quarks by sampling neutrino-candidates and evaluating the
    difference between the total missing transverse energy
    $E_\text{t}^\text{miss}$ and the candidates sum of
    transverse momenta.

    The final state of the leading process
    $t \overline{t} \to b \overline{b} \ell^+\ell^-\nu\overline{\nu}$,
    shown in figure \ref{fig:dilepton-diagram}, is described by \num{24}
    kinematic variables, \num{4} for each particle.
    The 4-vectors describing the two $b$-quarks and the charged leptons
    can be measured directly.
    The remaining \num{8} variables can be reduced to \num{6}, by neglecting
    the neutrino-mass such that one receives a total of \num{6}
    unknown variables.

    To determine these variables, the top-mass $m_t$ and the $W^\pm$-mass
    $m_W$ can be set to be the sum of the corresponding daughter-particles.
    In addition, the missing transverse energy has to match the transverse momenta of
    both neutrinos:
    \begin{align*}
        m_t^2 &= (p_{\nu_{1/2}} + p_\ell + p_b)^2\ \,, & m_W^2 &= (p_{\nu_{1/2}} + p_\ell)^2\,,\\
        E_x^\text{miss} &= p_x^{\nu_1} + p_x^{\nu_2} \,, & E_y^\text{miss} &= p_y^{\nu_1} + p_y^{\nu_2}\,.
    \end{align*}
    Here, $p_i = (E_i, \vec{p}_i)^\mathrm{T}$ is the 4-vector of the particle $i$.
    The two remaining neutrino variables can be obtained by selecting a certain value
    of pseudo-rapidity $\eta_\nu$ for both neutrinos. This yields both neutrino 4-vectors.
    To decide which $\eta_\nu$ to chose, a large number of values is tried
    and a weight $w$ for each corresponding candidate is calculated:
    \begin{equation}
        \label{eq:weight1}
        w = \exp\left[\frac{(E_x^\text{miss} - (p_x^{\nu_1} + p_x^{\nu_2}))^2}{2\sigma_x^2}\right]
        \cdot \exp\left[\frac{(E_y^\text{miss} - (p_y^{\nu_1} + p_y^{\nu_2}))^2}{2\sigma_y^2}\right]\,.
    \end{equation}
    Finally the two candidates are chosen, for which the weight is at maximum.
    More detailled explanation of these calculations is given in reference \cite{thesis-bonn}.

    \subsection{$\eta_\nu$-sampling and Top-mass-smearing}
    \label{subsec:sampling}
    The equations mentioned above yield zero to two solutions of $p^\nu$ for
    each neutrino. This number of solutions depends on the value assumed for $m_t$,
    which can be measured on average but fluctuates in single events according
    to a Breit-Wigner-distribution. Therefore the top-mass can either be smeared
    or extracted from the true top-quark during the reconstruction for testing.
    The difference of these reconstructions is shown in figure
    \ref{fig:topTruth-comparison}.
    The influence of the correct $W^\pm$-mass assumptions is negligible.
    \begin{figure}
        \centering
        \includegraphics[width=0.5\linewidth]{img/topTruth_comparison_m.pdf}
        \caption{
            Top-mass-distribution of reconstructed top-masses, based on sampled
            (green) and true (blue) top-mass assumptions, compared to the
            true top-mass-distribution.
            The difference between true and reconstructed distribution
            can be explained by wrong selections of
            neutrino-candidates due to inaccurate sampling of $\eta_\nu$-values.
        }
        \label{fig:topTruth-comparison}
    \end{figure}

    The performance of the reconstruction depends on the $\eta_\nu$-
    and $m_t$-sampling, which is shown in figure \ref{fig:performance}.
    In a first approach the top-mass is sampled according to a
    breit-wigner-distribution while $\eta_\nu$ is sampled with a gaussian
    distribution. The parameters of both functions are determined by a
    fit to the true distributions respectively.
    \begin{figure}
        \centering
        \begin{subfigure}{.49\linewidth}
            \centering
            \includegraphics[width=\linewidth]{img/performance_eta.pdf}
            \caption{
                Relative difference of reconstructed and true top-mass, normalized to
                the true top-mass. The differences decrease for larger size $N_\eta$
                of $\eta_\nu$-samples.
            }
            \label{fig:performance-a}
        \end{subfigure}
        \begin{subfigure}{.49\linewidth}
            \centering
            \includegraphics[width=\linewidth]{img/eta_sample_comparison_eta.pdf}
            \caption{
                Difference of reconstructed and true values of $\eta_\nu$.
                Here, the top-mass was fixed to the true, reconstructed value.
                The differences decrease for larger samples of $\eta_\nu$.
            }
            \label{fig:performance-b}
        \end{subfigure}
        \caption{
            Comparison of performance of top-mass-reconstruction with
            different sample sizes.
            The distributions are normalized to the total number of events
            respectively.
        }
        \label{fig:performance}
    \end{figure}

    Although it should be possible to always find the right $\eta_\nu$-values
    (with a weight of $w = \num{1}$)
    with arbitrarily big sample size $N_\eta$, the computing-time consumed by this
    calculations increases with $N_\eta^2$ and limits this approach drastically.
    As a consequence, some neutrinos are reconstructed with a huge deviation
    to the true value $\eta_\nu$.
    Looking at the $\eta_\nu$-distributions of single events clarifies this behavior.
    Some exemplary distributions are shown in figures \ref{fig:eta-dist-a} and
    \ref{fig:eta-dist-b}.
    Apparently there are up to four islands of huge weights $w$, which is a
    result of the number of possible solutions for the neutrino-momentum per
    $\eta_\nu$-value.
    If the true value of $\eta_\nu$ ($\eta_{\nu,\mathrm{truth}}$) is just in between two
    sampled values of $\eta_\nu$, it is possible that a maximum weight is
    found far away from this true value.
    This results in a wrong value of $\eta_\nu$ for the reconstructed neutrino.
    This behavior occurs more often in areas of low sample density and therefore
    an equidistant sample was tested in addition to the Gaussian distributed
    $\eta_\nu$ sample. This is depicted in figure \ref{fig:eta-dist-c} and
    \ref{fig:eta-dist-d}.

    Instad of using the weight, defined in equation \ref{eq:weight1}, one can
    use a slightly modified weight $w^\prime$ to select the $\nu$-candidates:
    \begin{equation}
        \label{eq:weight2}
        w^\prime = \exp\left[
            \frac{\sqrt{\left|E_x^\text{miss} - (p_x^{\nu} + p_x^{\overline{\nu}})\right|}}{2\sigma_x^2}
        \right]
        \cdot \exp\left[
            \frac{\sqrt{\left|E_y^\text{miss} - (p_y^{\nu} + p_y^{\overline{\nu}})\right|}}{2\sigma_y^2}
        \right]\,.
    \end{equation}
    This results in a smoother weight-distribution and is positive in the whole area
    of possible $\eta_\nu$ values. This makes the usage of a minimization algorithm
    possible, which is discussed in the outlook.
    Exemplary plots of such a distribution are shown in figure \ref{fig:eta-dists-zoom}.
    \begin{figure}[h]
        \centering
        \begin{subfigure}{.49\linewidth}
            \centering
            \includegraphics[width=\linewidth]{img/eta-dist1.png}
            \caption{}
            \label{fig:eta-dist-a}
        \end{subfigure}
        \begin{subfigure}{.49\linewidth}
            \centering
            \includegraphics[width=\linewidth]{img/eta-dist3.png}
            \caption{}
            \label{fig:eta-dist-b}
        \end{subfigure}
        \begin{subfigure}{.49\linewidth}
            \centering
            \includegraphics[width=\linewidth]{img/eta-dist5.png}
            \caption{}
            \label{fig:eta-dist-c}
        \end{subfigure}
        \begin{subfigure}{.49\linewidth}
            \centering
            \includegraphics[width=\linewidth]{img/eta-dist6.png}
            \caption{}
            \label{fig:eta-dist-d}
        \end{subfigure}
        \caption{
            Examples of $\eta_\nu$-samples of single events for which the weight is given as discribed
            in equation \ref{eq:weight1} (for \ref{fig:eta-dist-a} and \ref{fig:eta-dist-b})
            and equation \ref{eq:weight2} (for \ref{fig:eta-dist-c} and \ref{fig:eta-dist-d}).
            Reconstructed values of $\eta_\nu$ are marked with + and true values $\eta_{\nu,\mathrm{truth}}$
            are marked with \ast.
        }
        \label{fig:eta-dists}
    \end{figure}
    \begin{figure}[h]
        \centering
        \begin{subfigure}{.49\linewidth}
            \centering
            \includegraphics[width=\linewidth]{img/eta-dist-zoom1.png}
            \caption{}
            \label{fig:eta-dist-zoom-a}
        \end{subfigure}
        \begin{subfigure}{.49\linewidth}
            \centering
            \includegraphics[width=\linewidth]{img/eta-dist-zoom2.png}
            \caption{}
            \label{fig:eta-dist-zoom-b}
        \end{subfigure}
        \caption{
            Examples of $\eta_\nu$-samples with resulting reconstructed
            $\eta_\nu$ (marked with +) and true $\eta_{\nu,\mathrm{truth}}$
            (marked with \ast).
            The shown weight is modiefied as shown in equation \ref{eq:weight2}
            and the histogram zoomed to the area of possible values of $\eta$.
        }
        \label{fig:eta-dists-zoom}
    \end{figure}

    \section{Conclusion and Outlook}
    \label{sec:conclusion}
    The project presented here leads to a deeper understanding of the importance of
    different sampling approaches for the neutrino-weighting method.
    
    With further investigation on the feasability of using minimization techniques
    in combination with a modified weight, like $w^\prime$, the reconstruction
    of dilepton-events could be greatly improved. Therefore it has to be proved
    that the area of possible $\eta_\nu$-values can be determined analytically
    which leads to the rectangle areas of $\eta$ values mentioned above.
    It should furthermore be possible
    to reconstruct all such events in leading order monte-carlo data.
    This could lead to enhancements of data reconstruction in the
    dilepton-channel in future ATLAS analyzes.

    \printbibliography
\end{document}
