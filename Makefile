TEXOPTIONS = -lualatex \
			 --output-directory=build \
			 --interaction=nonstopmode \
			 --halt-on-error

TEXHEADERS = head.tex

IMAGES = img/*.pdf

all: build/report.pdf

build/report.pdf: report.tex head.tex lit.bib $(IMAGES) | build
	latexmk $(TEXOPTIONS) report.tex

build:
	mkdir -p build

clean:
	rm -r build
